//
//  MMAudioUtils.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/17/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMAudioUtils.h"

@implementation MMAudioUtils

+ (void)checkStatus:(OSStatus)result
            message:(const char *)errorMessage {
    if (result == noErr) return;
    
    char errorString[20];
    
    // See if it appears to be a 4-char-code *(UInt32 *)(errorString + 1) =
    CFSwapInt32HostToBig(result);
    if (isprint(errorString[1]) && isprint(errorString[2]) &&
        isprint(errorString[3]) && isprint(errorString[4])) {
        
        errorString[0] = errorString[5] = '\''; errorString[6] = '\0';
    } else {
        // No, format it as an integer sprintf(errorString, "%d", (int)error);
        fprintf(stderr, "Error: %s (%s)\n", errorMessage, errorString);
    }
}

+ (SInt16 **)shortBuffersWithNumberOfFrames:(UInt32)frames
                           numberOfChannels:(UInt32)channels {
    size_t size = sizeof(SInt16 *) * channels;
    SInt16 **buffers = (SInt16 **)malloc(size);
    for (int i = 0; i < channels; i++)
    {
        size = sizeof(SInt16) * frames;
        buffers[i] = (SInt16 *)malloc(size);
    }
    return buffers;
}

+ (void)freeShortBuffers:(SInt16 **)buffers
        numberOfChannels:(UInt32)channels {
    if (!buffers || !*buffers) {
        return;
    }
    
    for (int i = 0; i < channels; i++) {
        free(buffers[i]);
    }
    free(buffers);
}

+ (AudioBufferList *)audioBufferListWithNumberOfFrames:(UInt32)frames
                                      numberOfChannels:(UInt32)channels {
    unsigned nBuffers;
    unsigned bufferSize;
    unsigned channelsPerBuffer;
    
    nBuffers = channels;
    bufferSize = sizeof(SInt16) * frames;
    channelsPerBuffer = 1;
    
    AudioBufferList *audioBufferList = (AudioBufferList *)malloc(sizeof(AudioBufferList) + sizeof(AudioBuffer) * (channels-1));
    audioBufferList->mNumberBuffers = nBuffers;
    for(unsigned i = 0; i < nBuffers; i++) {
        audioBufferList->mBuffers[i].mNumberChannels = channelsPerBuffer;
        audioBufferList->mBuffers[i].mDataByteSize = bufferSize;
        audioBufferList->mBuffers[i].mData = calloc(bufferSize, 1);
    }
    return audioBufferList;
}

+ (void)freeBufferList:(AudioBufferList *)bufferList {
    if (bufferList) {
        if (bufferList->mNumberBuffers) {
            for( int i = 0; i < bufferList->mNumberBuffers; i++) {
                if (bufferList->mBuffers[i].mData) {
                    free(bufferList->mBuffers[i].mData);
                }
            }
        }
        free(bufferList);
    }
    bufferList = NULL;
}

@end
