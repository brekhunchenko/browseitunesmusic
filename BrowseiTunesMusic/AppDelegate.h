//
//  AppDelegate.h
//  BrowseiTunesMusic
//
//  Created by Yaroslav on 9/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

