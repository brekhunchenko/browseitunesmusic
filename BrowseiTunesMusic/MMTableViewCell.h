//
//  MMTableViewCell.h
//  BrowseiTunesMusic
//
//  Created by Yaroslav on 9/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MMTableViewCell : UITableViewCell

@property (nonatomic, strong) MPMediaItem* mediaItem;

@end
