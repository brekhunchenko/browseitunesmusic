//
//  MMPlaybackTimeInterval.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMPlaybackTimeInterval.h"

@interface MMPlaybackTimeInterval()

@property (nonatomic, assign, readwrite) NSTimeInterval startTime;
@property (nonatomic, assign, readwrite) NSTimeInterval endTime;

@end

@implementation MMPlaybackTimeInterval

#pragma mark - Initializers

+ (instancetype)playbackTimeIntervalWithStartTime:(NSTimeInterval)startTime
                                          endTime:(NSTimeInterval)endTime {
    MMPlaybackTimeInterval* playbackTimeInterval = [MMPlaybackTimeInterval new];
    playbackTimeInterval.startTime = startTime;
    playbackTimeInterval.endTime = endTime;
    return playbackTimeInterval;
}

+ (instancetype)playbackTimeIntervalWithSecondsFromTime:(NSTimeInterval)startTime
                                                seconds:(NSTimeInterval)seconds {
    MMPlaybackTimeInterval* playbackTimeInterval = [MMPlaybackTimeInterval new];
    playbackTimeInterval.startTime = startTime;
    playbackTimeInterval.endTime = playbackTimeInterval.startTime + seconds;
    return playbackTimeInterval;
}

#pragma mark - Class Methods

- (NSTimeInterval)numberOfSecondsInTimeInterval {
    NSTimeInterval startTimeInSeconds = self.startTime;
    NSTimeInterval endTimeInSeconds = self.endTime;
    return endTimeInSeconds - startTimeInSeconds;
}

@end
