//
//  MMAudioUtils.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/17/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MMAudioUtils : NSObject

+ (void)checkStatus:(OSStatus)result
            message:(const char *)message;

+ (SInt16 **)shortBuffersWithNumberOfFrames:(UInt32)frames
                           numberOfChannels:(UInt32)channels;
+ (void)freeShortBuffers:(SInt16 **)buffers
        numberOfChannels:(UInt32)channels;

+ (AudioBufferList *)audioBufferListWithNumberOfFrames:(UInt32)frames
                                      numberOfChannels:(UInt32)channels;
+ (void)freeBufferList:(AudioBufferList *)bufferList;

@end
