//
//  ViewController.m
//  BrowseiTunesMusic
//
//  Created by Yaroslav on 9/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "ViewController.h"
#import "MMTableViewCell.h"
#import "WaveformDebugView.h"
#import "MMAudioWaveformReader.h"
#import "MMPlaybackTimeInterval.h"

#import <MediaPlayer/MediaPlayer.h>

@interface ViewController () < UITableViewDelegate, UITableViewDataSource >

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet WaveformDebugView *waveformDebugView;

@property (nonatomic, strong) NSArray* mediaItems;

@end

@implementation ViewController

#pragma mark - 
#pragma mark - UIViewController Lifecycle

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self _loadTracksWithNames:@[@"Astral Romance", @"As You Are"]];
}

#pragma mark - Helpers

- (void)_loadTracksWithNames:(NSArray<NSString *> *)listOfTrackNames {
  MPMediaQuery *searchQuery = [[MPMediaQuery alloc] init];
  [searchQuery addFilterPredicate:[MPMediaPropertyPredicate predicateWithValue:[NSNumber numberWithBool:YES]
                                                                   forProperty:MPMediaItemPropertyIsCloudItem]];
  _mediaItems = searchQuery.items;
//  for (MPMediaItem* mediaItem in [searchQuery items]) {
//    if ([mediaItem valueForProperty:MPMediaItemPropertyAssetURL]) {
//      NSString* trackName = [mediaItem valueForKey:MPMediaItemPropertyTitle];
//      if ([listOfTrackNames indexOfObject:trackName] != NSNotFound) {
//        _mediaItems = [_mediaItems arrayByAddingObject:mediaItem];
//      }
//    }
//  }
}

#pragma mark - 
#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  MMTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MMTableViewCell class])];
  MPMediaItem* item = _mediaItems[indexPath.row];
  cell.mediaItem = item;
  return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [_mediaItems count];
}

#pragma mark - 
#pragma mark - UITableView DataSource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  MPMediaItem* item = _mediaItems[indexPath.row];
  NSURL *url = [item valueForProperty:MPMediaItemPropertyAssetURL];
  MMAudioWaveformReader* waveformReader = [[MMAudioWaveformReader alloc] initWithURL:url];
  MMPlaybackTimeInterval* timeInterval = [MMPlaybackTimeInterval playbackTimeIntervalWithStartTime:4.0f
                                                                                           endTime:9.0f];
  [_waveformDebugView setWaveformData:[waveformReader readWaveformDataAtTimeInterval:timeInterval]];
}

@end
