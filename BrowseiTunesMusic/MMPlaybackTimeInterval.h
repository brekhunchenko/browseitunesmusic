//
//  MMPlaybackTimeInterval.h
//  WindAudioEngine
//
//  Created by Yaroslav on 8/18/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@interface MMPlaybackTimeInterval : NSObject

+ (instancetype)playbackTimeIntervalWithStartTime:(NSTimeInterval)startTime
                                          endTime:(NSTimeInterval)endTime;

+ (instancetype)playbackTimeIntervalWithSecondsFromTime:(NSTimeInterval)startTime
                                                seconds:(NSTimeInterval)seconds;

@property (nonatomic, assign, readonly) NSTimeInterval startTime;
@property (nonatomic, assign, readonly) NSTimeInterval endTime;

- (NSTimeInterval)numberOfSecondsInTimeInterval;

@end
