//
//  MMTableViewCell.m
//  BrowseiTunesMusic
//
//  Created by Yaroslav on 9/26/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMTableViewCell.h"

@interface MMTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *artistLabel;

@end

@implementation MMTableViewCell

#pragma mark - Custom Setters & Getters

- (void)setMediaItem:(MPMediaItem *)mediaItem {
  _mediaItem = mediaItem;
  
  _artistLabel.text = [NSString stringWithFormat:@"%@", _mediaItem.title];
}
@end
