//
//  MMAudioWaveformReader.m
//  WindAudioEngine
//
//  Created by Yaroslav on 8/22/16.
//  Copyright © 2016 Yalex Mobile. All rights reserved.
//

#import "MMAudioWaveformReader.h"
#import <AVFoundation/AVFoundation.h>

#import "MMAudioUtils.h"
#import "MMPlaybackTimeInterval.h"

typedef struct {
    AudioFileID audioFileID;
    AudioStreamBasicDescription clientFormat;
    NSTimeInterval duration;
    ExtAudioFileRef extAudioFileRef;
    AudioStreamBasicDescription fileFormat;
    SInt64 frames;
    CFURLRef fileURL;
} MMAudioInfo;

@interface MMAudioWaveformReader()

@property (nonatomic, assign) MMAudioInfo *info;
@property (nonatomic, assign) SInt16 **shortData;

@end

@implementation MMAudioWaveformReader

#pragma mark - Initializers

- (instancetype)initWithURL:(NSURL *)fileURL {
    self = [super init];
    if (self) {
        _fileURL = fileURL;
        
        self.info = (MMAudioInfo *)malloc(sizeof(MMAudioInfo));
        self.info->fileURL = (__bridge CFURLRef)fileURL;
        AudioStreamBasicDescription audioDescription;
        audioDescription.mSampleRate = [AVAudioSession sharedInstance].sampleRate;
        audioDescription.mFormatID = kAudioFormatLinearPCM;
        audioDescription.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
        audioDescription.mChannelsPerFrame = 1;
        audioDescription.mBitsPerChannel = 16;
        audioDescription.mBytesPerFrame = sizeof(SInt16);
        audioDescription.mBytesPerPacket = sizeof(SInt16);
        audioDescription.mFramesPerPacket = 1;
        self.info->clientFormat = audioDescription;
        
        CFURLRef url = self.info->fileURL;
        [MMAudioUtils checkStatus:ExtAudioFileOpenURL(url, &self.info->extAudioFileRef)
                          message:"Open audio failed."];
        
        UInt32 propSize = sizeof(self.info->audioFileID);
        [MMAudioUtils checkStatus:ExtAudioFileGetProperty(self.info->extAudioFileRef,
                                                              kExtAudioFileProperty_AudioFile,
                                                              &propSize,
                                                              &self.info->audioFileID)
                            message:"Get AudioFileID failed."];
        
        propSize = sizeof(self.info->fileFormat);
        [MMAudioUtils checkStatus:ExtAudioFileGetProperty(self.info->extAudioFileRef,
                                                          kExtAudioFileProperty_FileDataFormat,
                                                          &propSize,
                                                          &self.info->fileFormat)
                            message:"Failed to get file audio format on existing audio file."];
        
        propSize = sizeof(SInt64);
        [MMAudioUtils checkStatus:ExtAudioFileGetProperty(self.info->extAudioFileRef,
                                                          kExtAudioFileProperty_FileLengthFrames,
                                                          &propSize,
                                                          &self.info->frames)
                            message:"Failed to get total frames."];
        
        self.info->duration = (NSTimeInterval) self.info->frames/self.info->fileFormat.mSampleRate;
        
        self.info->clientFormat = audioDescription;
        
        [MMAudioUtils checkStatus:ExtAudioFileSetProperty(self.info->extAudioFileRef,
                                                              kExtAudioFileProperty_ClientDataFormat,
                                                              sizeof(audioDescription),
                                                              &audioDescription)
                            message:"Couldn't set client data format on file."];

    }
    return self;
}

#pragma mark - 

- (void)dealloc {
    free(self.info);
}

#pragma mark - Class Methods

- (NSData *)readWaveformDataAtTimeInterval:(MMPlaybackTimeInterval *)timeInterval {
    if ([self _isValidPlaybackTimeInterval:timeInterval] == NO) {
        NSLog(@"Invalid time interval.");
        return nil;
    }
    
    UInt32 channels = self.info->clientFormat.mChannelsPerFrame;
    SInt64 framesOffset = [self _numberOfFramesInSeconds:timeInterval.startTime];
    [MMAudioUtils checkStatus:ExtAudioFileSeek(self.info->extAudioFileRef, framesOffset)
                      message:"Failed to seek frame position within audio file."];
    
    UInt32 bufferSize = [self _bufferSize];
    NSTimeInterval timeIntervalNumberOfSeconds = [timeInterval numberOfSecondsInTimeInterval];
    AudioBufferList *audioBufferList = [MMAudioUtils audioBufferListWithNumberOfFrames:(UInt32)bufferSize
                                                                      numberOfChannels:self.info->clientFormat.mChannelsPerFrame];
    NSMutableData* dataMutable = [NSMutableData new];
    for (SInt64 i = 0; i < [self _numberOfFramesInSeconds:timeIntervalNumberOfSeconds]; i++) {
        [MMAudioUtils checkStatus:ExtAudioFileRead(self.info->extAudioFileRef,
                                                   &bufferSize,
                                                   audioBufferList)
                          message:"Failed to read audio data from file waveform"];

        for (int channel = 0; channel < channels; channel++) {
            SInt16 *channelData = audioBufferList->mBuffers[channel].mData;
            [dataMutable appendBytes:channelData
                              length:audioBufferList->mBuffers[channel].mDataByteSize];
        }
    }
    [MMAudioUtils freeBufferList:audioBufferList];
    
    return dataMutable;
}

- (NSTimeInterval)audioDuration {
    return self.info->duration;
}

#pragma mark - Helpers

- (SInt16)_numberOfFramesInSeconds:(NSTimeInterval)seconds {
    AudioStreamBasicDescription clientFormat = self.info->clientFormat;
    return seconds*clientFormat.mSampleRate/[self _bufferSize];
}

- (SInt64)_totalNumberOfFrames {
    SInt64 totalFrames = [self _totalFrames];
    AudioStreamBasicDescription clientFormat = self.info->clientFormat;
    AudioStreamBasicDescription fileFormat = self.info->fileFormat;
    BOOL sameSampleRate = clientFormat.mSampleRate == fileFormat.mSampleRate;
    if (!sameSampleRate) {
        totalFrames = self.info->duration*clientFormat.mSampleRate;
    }
    return totalFrames;
}

- (SInt64)_totalFrames {
    return self.info->frames;
}

- (SInt16)_bufferSize {
    return 1024;
}

- (BOOL)_isValidPlaybackTimeInterval:(MMPlaybackTimeInterval *)playbackTimeInterval {
    if (playbackTimeInterval.startTime >= playbackTimeInterval.endTime) {
        return NO;
    }
    if (self.info->duration < playbackTimeInterval.endTime) {
        return NO;
    }
    return YES;
}

@end
